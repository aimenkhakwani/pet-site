# _Bella The Westie_

#### _This is a blog about our westie, Bella_, 08/02/16_

#### By _**Aimen K. Ayana P.**_

## Description
_This is a blog about our Bella, the westie. Enjoy!_

## Setup/Installation Requirements
* _Clone from github.com/bella_

## Known Bugs
_Not compatible with Windows7_

## Support and contact details
_email for additional setup instructions through github_

## Technologies Used
_HTML, css, Javascript, Jquery_

### License
* Epicodus

Copyright (c) 2016 **_Aimen K Production_**
